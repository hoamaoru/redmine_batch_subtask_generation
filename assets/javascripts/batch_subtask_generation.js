
$(function () {
    $(document).ready(function () {

        const URL = {
            ISSUES: '/issues.json',
            API_KEY: '/my/api_key',
            DELETE_ISSUE: '/issues/'
        }
        // Define tracker
        const TRACKERS = {
            TRANSLATE: 4,
            CODING: 5,
            SPECT_ANALYS: 6,
            REVIEW: 11,
            UNIT_TEST: 10,
            BUG_MANAGEMENT: 15,
            INTEGRATION_TESTING: 12,
            QA: 7
        }

        var addedIssues = [];

        var projectId = $('#issue_project_id').val();
        // var projectId = $('#project-jump span.drdn-trigger').text();

        var subject = $('#issue_subject').val();
        var priorityId = $('#issue_priority_id').val();
        var assignedToId = $('#issue_assigned_to_id').val();
        var fixedVersionId = $('#issue_fixed_version_id').val();
        var startDate = $('.start-date .value').text();

        var btnGenSubtasks = $('<a style="margin-left: 10px" title="Generate Subtasks" href="#">Generate</a>');
        $('#issue_tree > p').append(btnGenSubtasks);

        btnGenSubtasks.on('click', function (e) {
            e.preventDefault();

            if (!confirm('Are you sure?')) {
                return;
            }
            let childrenIssues = getChildrenIssues();
            let parentIssueId = getCurrentIssueId();
            createChildrenIssues(childrenIssues, parentIssueId)
            .then(() => {
                location.reload();
            })
            .catch(function (e) {
                setCookie('redmine_api_key', '');
                deleteIssuesIfError();
                displayError();
                console.log(e);
            });
        });

        setCookie('redmine_api_key', '');


        /**
         * Create children issues
         * 
         * @param {*} issues 
         */
        async function createChildrenIssues(issues, parentIssueId) {

            for (let i = 0; i < issues.length; i++) {
                let childIssue = setChildIssueInfo(issues[i]['tracker_id'], parentIssueId);
                // Call api create issue
                let data = await apiCreateIssue(childIssue);
                addedIssues.push(data.issue.id);
                if (Object.keys(issues[i]).includes('children')) {
                    await createChildrenIssues(issues[i]['children'], data.issue.id);
                }
            }
            return true;
        }

        /**
         * Add extra info for create child issue
         * 
         * @param {*} trackerId 
         * @param {*} parentIssueId 
         * @returns object
         */
        function setChildIssueInfo(trackerId, parentIssueId) {
            let issue = {};

            if (assignedToId) {
                issue['assigned_to_id'] = assignedToId;
            }
            if (trackerId) {
                issue['tracker_id'] = trackerId;
            }
            if (subject) {
                issue['subject'] = subject;
            }
            if (projectId) {
                issue['project_id'] = projectId;
            }
            if (priorityId) {
                issue['priority_id'] = priorityId;
            }
            if (fixedVersionId) {
                issue['fixed_version_id'] = fixedVersionId;
            }
            if (parentIssueId) {
                issue['parent_issue_id'] = parentIssueId;
            }
            // if (startDate) {
            //     issue['start_date'] =  formatDate(startDate);
            // }
            return { issue };
        }

        /**
         * Call redmine API to create issue
         * 
         * @param {*} issue 
         * @param {*} parentIssueId 
         * @returns Promise
         */
        async function apiCreateIssue(issue) {
            // Get redmine API key
            let redmineApiKey = await getRedmineApiKey();
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: URL.ISSUES,
                    headers: {
                        'X-Redmine-API-Key': redmineApiKey
                    },
                    contentType: 'application/json',
                    data: JSON.stringify(issue),
                    success: function (data) {
                        resolve(data);
                    },
                    error: function (e) {
                        reject(e);
                    }
                });
            });
        }

        /**
         * Delete issues if error
         * 
         * @returns void
         */
        function deleteIssuesIfError() {
            if (addedIssues.length == 0) {
                return;
            }
            addedIssues.map((issueId) => {
                let promise = apiDeleteIssue(issueId);
                promise.then(() => {
                    addedIssues = addedIssues.filter(val => val != issueId);
                });
            });
        }

        async function apiDeleteIssue (issueId) {
            let promise = getRedmineApiKey();
            promise.then((redmineApiKey) => {
                $.ajax({
                    type: 'DELETE',
                    url: URL.DELETE_ISSUE + issueId + '.json',
                    headers: {
                        'X-Redmine-API-Key': redmineApiKey
                    },
                    contentType: 'application/json'
                });
                addedIssues = addedIssues.filter(val => val != issueId);
            });
            return promise;
        }

        /**
         * Set children issues by tracker
         * 
         * @returns array
         */
        function getChildrenIssues() {
            // TODO: Create api to get list issues
            return [
                { 'tracker_id': TRACKERS.TRANSLATE },
                { 'tracker_id': TRACKERS.SPECT_ANALYS },
                { 'tracker_id': TRACKERS.QA },
                {
                    'tracker_id': TRACKERS.CODING,
                    'children': [
                        { 'tracker_id': TRACKERS.CODING },
                        { 'tracker_id': TRACKERS.REVIEW },
                        { 'tracker_id': TRACKERS.UNIT_TEST },
                        { 'tracker_id': TRACKERS.BUG_MANAGEMENT }
                    ]
                },
                { 'tracker_id': TRACKERS.INTEGRATION_TESTING },
                { 'tracker_id': TRACKERS.BUG_MANAGEMENT }
            ];
        }

        /**
         * Get redmine API key 
         * 
         * @returns 
         */
        function getRedmineApiKey() {
            
            return new Promise(function (resolve, reject) {
                let redmineApiKey = getCookie('redmine_api_key');
                if (redmineApiKey) {
                    return resolve(redmineApiKey);
                }
                $.ajax({
                    type: 'GET',
                    url: URL.API_KEY,
                    contentType: 'text/javascript charset=utf-8',
                    success: function (html) {
                        let api_key = crawApiKey(html);
                        setCookie('redmine_api_key', api_key);
                        resolve(api_key);
                    },
                    error: function (e) {
                        reject(e);
                    }
                });
            });
        }

        function setCookie(key, value) {
            document.cookie = key + '=' + value + '; max-age=86400; path=/;';
            return true;
        }

        function getCookie(key) {
            let cookie = document.cookie
                .split('; ')
                .find(row => row.startsWith(key + '='));
            if (cookie) {
                return cookie.split('=')[1];
            }
            return null;
        }

        function getCurrentIssueId() {
            var url = window.location.href;
            var m = /issues\/(\d+)/.exec(url);
            if (m) {
                return m[1];
            }
            return null;
        }

        function crawApiKey (html) {
            return $(html).find('div.box pre').text();
        }

        function displayError () {
            let contentEle = $('#content');
            let errorEle = $('#flash_error');
            let errorMsg = 'An error occured!';
            if (errorEle.length == 0) {
                contentEle.prepend('<div class="flash error" id="flash_error">'+ errorMsg +'</div>')
            } else {
                errorEle.html(errorMsg);
            }
        }

        function formatDate (date) {
            let d = new Date(date);
            return d.getFullYear()+ "-" + ("0"+(d.getMonth()+1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2);
        }
    })
});