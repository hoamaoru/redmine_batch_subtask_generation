
require_dependency 'hooks/batch_subtask_generation_hook_listener'
Redmine::Plugin.register :redmine_batch_subtask_generation do
  name 'Redmine Batch Subtask Generation plugin'
  author 'balx'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url '#'
  author_url '#'
end
