class BatchSubtaskGenerationHookListener < Redmine::Hook::ViewListener
    render_on :view_issues_show_details_bottom, partial: "batch_subtask_generation/view_issues_show_details_bottom" 
end